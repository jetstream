/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function outputTemplate(file, data, type) {
    let template = readFile(file)
    if (type.match('mfs')) {
      res.setContentType('vnd.mozilla.xul+xml')
    } else {
      res.setContentType('text/html')
    }
    res.getWriter().print(template.process(data))
}

function readFile(file) {
    let(contents = new java.lang.StringBuffer(),
        input = new java.io.BufferedReader(new java.io.FileReader(file)), 
        line = null) {
        while (line = input.readLine()) { contents.append(line + '\n') }
        return new String(contents)
    }
}
