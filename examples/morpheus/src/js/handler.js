/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

importPackage(org.jetstream.js)

// internal dependencies
load('/icc/tools/jetstream/src/js/jetstream/template.js')
load('/icc/tools/jetstream/src/js/jetstream/routes.js')
load('/icc/tools/jetstream/src/js/jetstream/db.js')

load('/icc/tools/jetstream/src/js/jst.js')
load('/icc/tools/jetstream/examples/morpheus/src/js/util.js')

// base template directory
let templatesDir = '/icc/tools/jetstream/examples/morpheus/src/templates/'
let type = 'mfs/'

templatesDir += type;

// routes
let __HANDLER__ = jetstream.routes.handler
jetstream.routes.defroute({ pattern: /\/login(.*)/, capture: [], fn: _login })
jetstream.routes.defroute({ pattern: /\/post\/create/, capture: [], fn: _createPost })
jetstream.routes.defroute({ pattern: /\/ffoxlogin(.*)/, capture: [], fn: _ffoxlogin })

function _ffoxlogin(x) {
    x.channelName='geekd';
    x.password='ajtajt';
    x.template = 'ffoxlogin.jst';
    
    _login(x);
}

function _login(x) {
    let checkLogin = function() {
        withDB(function(s) {
            let q = 'select channel_name, channel_id, password from rbl_channel join ant_customer using(customer_id) where channel_name = \'${name}\''
            q = q.process({ name: x.channelName != null ? x.channelName : req.getParameter('channelName') })
            app.log.debug(q)
            let rs = s.executeQuery(q)
            if(rs.next()) {
                let dbpw = rs.getString('password')
                let pw = x.password != null ? "ajtajt" : req.getParameter('password')
                app.log.debug('pw = ${pw}, db pw = ${dbpw}'.process({ pw: pw, dbpw: dbpw })) 
                if(pw.equals(dbpw)) {
                    // successful login
                    session.channelID = rs.getString('channel_id')
                    jetstream.template.render(templatesDir, 'menu.jst', { channelName: rs.getString('channel_name') })
                    return
                }
            }
            jetstream.template.render(templatesDir, 'login.jst', {
                                                 channelName: req.getParameter('channelName'), 
                                                 error:'invalid username or password', 
                                                 action: app.fn(checkLogin, app.sessionManager.generateGUID() + '.xul')}, 1)
        })
    }
    jetstream.template.render(templatesDir, x.template != null ? x.template : 'login.jst', { action: app.fn(checkLogin, app.sessionManager.generateGUID() + '.xul') }, 1)
}

function _createPost(x) {
    let createPost = function() {
        app.log.debug('title = ${title}, body = ${body}'.process(x))
        if(x.action == 'Attach an Image') {
            _selectImage(function(mediaID, mfsMediaID) { _createPost({title: x.title, body: x.body, imageID: mfsMediaID}) })
        }
    }
    jetstream.template.render(templatesDir, 'create_post.jst', { action: app.fn(createPost, app.sessionManager.generateGUID() + '.xul'), title: x.title, body: x.body, image: x.imageID }, 1)
}

function _selectImage(fn) {
    let images = getImages().map(function(e) { 
        return { 
            mediaID: e.mediaID, 
            mfsMediaID: e.mfsMediaID, 
            href: app.fn(function() { fn(e.mediaID, e.mfsMediaID) }, app.sessionManager.generateGUID() + '.xul'),
        }
    })
    app.log.debug({ images: images }.dumper())
    jetstream.template.render(templatesDir, 'select_image.jst', { images: images })
}

function getImages() {
    let images = []
    withDB(function(s) {
        // get the images for the user
        let q = 'select media_id, mfs_media_id from rbl_media join rbl_media_source_mfs using (media_id) where channel_id = ${channelID} limit 5'
        q = q.process({ channelID: session.channelID })
        let rs = s.executeQuery(q)
        
        // build the datastructure
        while(rs.next()) {
            images.push({ 
                mediaID: rs.getString('media_id'),
                mfsMediaID: rs.getString('mfs_media_id'),
            })
        }
    })
    return images
}

function withDB(fn) {
    jetstream.db.withConnection('db02.rabbletest.com', 'dev_rabble2', 5432, 'rabble_writer', 'sb4dcb0!ze', function(c) {
        jetstream.db.withStatement(c, function(s) {
            return fn(s)
        })
    })
}
