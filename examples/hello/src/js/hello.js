// specify to ScriptableServlet what code to execute
var __HANDLER__ = { __GET__: handler, __POST__: handler }

function handler() {
    // set the content type
    res.setContentType('text/plain')
    
    // allocate an instance of our bundled class
    let h = new org.jetstream.hello.HelloWorld

    // invoke a method on it
    res.getWriter().println(h.getMessage())

    session.foo = session.foo + " baz " || "bar"
    res.getWriter().println(session.foo)
}
