/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// specify to ScriptableServlet what code to execute
var __HANDLER__ = { __GET__: fooHandler, __POST__: handler, __RESTART__: handler }


// internal dependencies
load('/icc/tools/jetstream/examples/myspace/src/js/myspace.js')
var myspace = icc.communities.myspace

var methods = {
    'icc.myspace.login': icc_myspace_login,
    'icc.myspace.comments.list': icc_myspace_comments_list,
    'icc.myspace.friends.list': icc_myspace_friends_list,
    'icc.myspace.bulletins.list': icc_myspace_bulletins_list,
    'icc.myspace.photos.list': icc_myspace_photos_list,
    'icc.myspace.posts.list': icc_myspace_posts_list,
    'icc.myspace.profile.get': icc_myspace_profile_get,
}

function fooHandler() {
        icc_myspace_login({ username: 'derrick@doien.com', password: 'luther_' })
    try {
    }
    catch(e if e instanceof org.apache.axis2.AxisFault) {
        res.getWriter().println('fault: ' + e.message)
    }
    catch(e) {
        res.getWriter().println('e: ' + e)
    }
}

function handler(fn) {
    let response
    try {
        let content = getRequestBody()
        app.log.debug(content)
        let request = eval('(' + content  + ')')
        
        if(! request.method) {
            throw "no method was supplied"
        }
        else if(! request.parameters || ! request instanceof Object) {
            throw "no parameters were supplied"
        }
        response = methods[request.method](request.parameters)
    }
    catch(e) {
        response = {
            exception: {
                message: e.toString()
            }
        }
    }
    
    app.log.debug('about to return\n\n\n' + response.dumper() + '\n\n\n')
    res.getWriter().println(response.dumper())
}

function getRequestBody() {
    let is = req.getInputStream()
    let bytes = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, 1024)
    let sb = new java.lang.StringBuffer()
    let bytesRead
    while((bytesRead = is.read(bytes)) != -1) {
        sb.append(new java.lang.String(bytes, 0, bytesRead))
    }
    return sb.toString()
}

// icc.myspace.login
function icc_myspace_login(parameters) {
    let [token, userID] = myspace.login(parameters.username, parameters.password)
    session.userID = userID
    session.token = token
    return { meta: { cardinality: ':singular' }, rv: { user_id: userID } }
}

// icc.myspace.comments.list
function icc_myspace_comments_list(parameters) {
    if(! session.token) {
        throw 'you must have a valid token to get a list of comments'
    }
    
    // default per_page and page_num
    parameters.per_page = parameters.per_page || 10
    parameters.page_num = parameters.page_num || 1

    let [comments, totalComments] = myspace.getProfileComments(parameters.user_id, session.token, parameters.per_page, parameters.page_num)
    return { meta: { cardinality: ':plural', total_items: totalComments }, rv: comments }
}

// icc.myspace.friends.list
function icc_myspace_friends_list(x) {
    x.per_page = x.per_page || 10
    x.page_num = x.page_num || 1

    let [friends, totalFriends] = myspace.getFriends(x.user_id, session.token, x.per_page, x.page_num)
    return { meta: { cardinality: ':plural', total_items: totalFriends }, rv: friends }
}

// icc.myspace.bulletins.list
function icc_myspace_bulletins_list(x) {
    x.per_page = x.per_page || 10
    x.page_num = x.page_num || 1

    let [bulletins, totalBulletins] = myspace.getBulletins(session.token, x.per_page, x.page_num)
    return { 
        meta: { cardinality: ':plural', total_items: totalBulletins }, 
        rv: bulletins 
    }
}

// icc.myspace.photos.list
function icc_myspace_photos_list(x) {
    x.per_page = x.per_page || 10
    x.page_num = x.page_num || 1

    let [photos, totalPhotos] = myspace.getPhotos(x.user_id, session.token, x.per_page, x.page_num)
    return { 
        meta: { cardinality: ':plural', total_items: totalPhotos }, 
        rv: photos 
    }
}

// icc.myspace.posts.list
function icc_myspace_posts_list(x) {
    x.per_page = x.per_page || 10
    x.page_num = x.page_num || 1

    let [posts, totalPosts] = myspace.getPosts(x.user_id, session.token, x.per_page, x.page_num)
    return { 
        meta: { cardinality: ':plural', total_items: totalPosts }, 
        rv: posts 
    }
}

// icc.myspace.profile.get
function icc_myspace_profile_get(x) {
    x.per_page = x.per_page || 10
    x.page_num = x.page_num || 1

    let profile = myspace.getProfile(x.user_id, session.token)
    return { 
        meta: { cardinality: ':singular' }, 
        rv: profile
    }
}
