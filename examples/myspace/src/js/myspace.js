/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// internal dependencies
importPackage(com.icc.axis2.myspace)

defpackage('icc.communities.myspace', function() {

this.login = function(username, password) {
    // get the token
    let securityService = new SecurityServiceStub()
    let request = new SecurityServiceStub.GenerateSaltAndHash()
    request.setClear(password)
    let response = securityService.GenerateSaltAndHash(request).getResults()

    let tokenRequestData = new SecurityServiceStub.TokenRequestData()
    tokenRequestData.setCredential(username)
    tokenRequestData.setSalt(response.getSalt())
    tokenRequestData.setHash(response.getHash())

    request = new SecurityServiceStub.RequestToken()
    request.setRequest(tokenRequestData)
    let token = securityService.RequestToken(request).getRequestTokenResult().getToken()
    
    // get the user id
    let r2 = new SecurityServiceStub.GetUserIDRequestData()
    r2.setToken(token)
    let r3 = new SecurityServiceStub.GetUserID()
    r3.setRequest(r2)
    let userID = securityService.GetUserID(r3).getGetUserIDResult().getUserID()

    return [token, userID]
}

this.getProfile = function(userID, token) {
    if(! userID) {
        throw 'no user id was supplied'
    }
    if(! token) {
        throw 'no token was supplied'
    }

    let profileService = new ProfileServiceStub()
    let m1 = new ProfileServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new ProfileServiceStub.MySpace()
    m2.setMySpace(m1)

    let r1 = new ProfileServiceStub.GetProfileByID()
    r1.setProfileID(userID)
    let r2 = profileService.GetProfileByID(r1, m2).getGetProfileByIDResult()
    
    return {
        display_name: r2.getDisplayName(),
        age: r2.getAge(),
        headline: r2.getHeadline(),
        gender: r2.getGender().toString(),
        city: r2.getCity(),
        image_url: r2.getImageUrl(),
        last_on: java.lang.String.format('%1$tm/%1$te/%1$tY', r2.getLastOn()),
    }
}

this.getProfileComments = function(userID, token, pageSize, currentPage) {
    if(! userID) {
        throw 'no user id was supplied'
    }
    if(! token) {
        throw 'no token was supplied'
    }

    let profileService = new ProfileServiceStub()
    let m1 = new ProfileServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new ProfileServiceStub.MySpace()
    m2.setMySpace(m1)
    let r1 = new ProfileServiceStub.GetComments()
    r1.setUserID(userID)
    r1.setPageSize(pageSize)
    r1.setCurrentPage(currentPage)
    let r2 = profileService.GetComments(r1, m2).getGetCommentsResult()
    
    let totalComments = r2.getPagingContext().getTotalCount()
    let comments = r2.getComment().map(function(e) {
        return {
            id: e.getId(),
            body: e.getBody(),
            date: java.lang.String.format('%1$tm/%1$te/%1$tY', e.getPostDate()),
            author: {
                display_name: e.getAuthor().getDisplayName(),
                id: e.getAuthor().getId(),
                image_url: e.getAuthor().getImageUrl(),
            }
        }
    })

    return [comments, totalComments]
}

this.getFriends = function(userID, token, pageSize, currentPage) {
    if(! userID) {
        throw 'no user id was supplied'
    }
    if(! token) {
        throw 'no token was supplied'
    }
    
    let friendService = new FriendServiceStub()
    let m1 = new FriendServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new FriendServiceStub.MySpace()
    m2.setMySpace(m1)
    let r1 = new FriendServiceStub.GetFriends()
    r1.setPage(currentPage)
    r1.setPageSize(pageSize)
    r1.setUserID(userID)

    let r2 = friendService.GetFriends(r1, m2).getGetFriendsResult()
    let totalFriends = r2.getPagingContext().getTotalCount()

    let friends = r2.getFriend().map(function(e) {
        return {
            id: e.getId(),
            image_url: e.getImageUrl(),
            display_name: e.getDisplayName()
        }
    })

    return [friends, totalFriends]
}

this.getBulletins = function(token, pageSize, currentPage) {
    if(! token) {
        throw 'no token was supplied'
    }
    
    let bulletinService = new BulletinServiceStub()
    let m1 = new BulletinServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new BulletinServiceStub.MySpace()
    m2.setMySpace(m1)
    let r1 = new BulletinServiceStub.GetBulletins()
    r1.setCurrentPage(currentPage)
    r1.setPageSize(pageSize)

    let r2 = bulletinService.GetBulletins(r1, m2).getBulletins()
    let totalBulletins = r2.getPagingContext().getTotalCount()
    let bulletins = r2.getBulletin().map(function(e) {
        return {
            id: e.getId().toString(),
            title: e.getTitle(),
            body: e.getBody() || '',
            date: java.lang.String.format('%1$tm/%1$te/%1$tY', e.getPostDate()),
            author: {
                id: e.getAuthor().getId(),
                image_url: e.getAuthor().getImageUrl(),
                display_name: e.getAuthor().getDisplayName()
            }
        }
    })
    
    return [bulletins, totalBulletins]
}

this.getPhotos = function(userID, token, pageSize, currentPage) {
    if(! userID) {
        throw 'no user id was supplied'
    }
    if(! token) {
        throw 'no token was supplied'
    }

    let photoService = new PhotoServiceStub()
    let m1 = new PhotoServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new PhotoServiceStub.MySpace()
    m2.setMySpace(m1)
    let r1 = new PhotoServiceStub.GetPhotos()
    r1.setCurrentPage(currentPage)
    r1.setPageSize(pageSize)
    r1.setOwnerID(userID)

    let r2 = photoService.GetPhotos(r1, m2).getGetPhotosResult()
    let totalPhotos = r2.getPagingContext().getTotalCount()
    let photos = r2.getPhoto().map(function(e) {
        return {
            image_url: e.getImageURL(),
            caption: e.getCaption() || ''
        }
    })
    
    return [photos, totalPhotos]
}

this.getPosts = function(userID, token, pageSize, currentPage) {
    if(! userID) {
        throw 'no user id was supplied'
    }
    if(! token) {
        throw 'no token was supplied'
    }

    let blogService = new BlogServiceStub()
    let m1 = new BlogServiceStub.MySpaceSoapHeader()
    m1.setToken(token)
    let m2 = new BlogServiceStub.MySpace()
    m2.setMySpace(m1)
    let r1 = new BlogServiceStub.GetPosts()
    r1.setCurrentPage(currentPage)
    r1.setPageSize(pageSize)
    r1.setUserID(userID)

    let r2 = blogService.GetPosts(r1, m2).getPosts()
    let totalPosts = r2.getPagingContext().getTotalCount()
    let posts = r2.getPost().map(function(e) {
        return {
            subject: e.getSubject(),
            date: java.lang.String.format('%1$tm/%1$te/%1$tY', e.getDate()),
        }
    })
    
    return [posts, totalPosts]
}

})
