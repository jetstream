/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// internal dependencies
load('/icc/tools/jetstream/src/js/db.js')

// specify to ScriptableServlet what code to execute
var __HANDLER__ = { __GET__: handler, __POST__: handler }

function handler() {
    withDB(function(s) {
        let q = 'select channel_id from rbl_channel limit 10'
        let rs = s.executeQuery(q)
        while(rs.next()) {
            res.getWriter().println(rs.getString('channel_id'))
        }
    })
}

function withDB(fn) {
    db.withConnection('db02.rabbletest.com', 'dev_rabble2', 5432, 'rabble_writer', 'sb4dcb0!ze', function(c) {
        db.withStatement(c, function(s) {
            return fn(s)
        })
    })
}
