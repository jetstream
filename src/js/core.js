/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function curry(fn, x) {
    return function(y) { return fn(x.merge(y)) }
}

function merge(x, y) { return x.merge(y) }
