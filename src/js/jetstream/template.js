/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// trimpath dependencies
load('/icc/tools/jetstream/src/js/jst.js')

// internal dependencies
load('/icc/tools/jetstream/src/js/jetstream/util.js')

defpackage('jetstream.template', function() {

// evaluates a template and outputs the result to standard out
this.render = function(dir, file, data) {
    let template = jetstream.util.readFile(dir + '/' + file)
    res.setContentType('text/html')
    res.getWriter().print(template.process(data))
}

})
