/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
defpackage('jetstream.db', function() {

// wraps a function call with a db connection to prevent the possibility of leaking the connection 
this.withConnection = function(server, db, port, user, password, fn) {
    let c = app.db.getConnection(server, db, port, user, password)
    let rv = fn(c)
    c.close()
    return rv
}

// likewise with a handle
this.withStatement = function(c, fn) {
    let s = c.createStatement()
    let rv = fn(s)
    s.close()
    return rv
}

})
