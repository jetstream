/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// internal dependencies
load('/icc/tools/jetstream/src/js/jetstream/util.js')

defpackage('jetstream.routes', function(__package__) {

__package__.handler = {
    __GET__: function() { jetstream.routes.dispatch() },
    __POST__: function() { jetstream.routes.dispatch() },
    __RESTART__: function(fn) { jetstream.routes.dispatchFn(fn) },
}

// initialize the routes datastructure
__package__.routes = []

// define a new route
__package__.defroute = function(x) {
    if(!x.pattern || !x.capture || !x.fn) {
        throw new JetStreamException('defroute takes named arguemts: pattern, capture, and fn')
    }

    __package__.routes.push({ 
        pattern: new RegExp("^\\" + app.config.getPathPrefix() + x.pattern.source + "$"), 
        capture: x.capture, 
        fn: x.fn 
    })
}

__package__.dispatch = function() {
    // get the URL
    let url = req.getRequestURI()

    // find the appropriate route
    let candidates = __package__.routes.filter(function(e) { return url.match(e.pattern) })
    if(candidates.length != 1) {
        res.setStatus(404)
        res.getWriter().println('page not found')
        return
    }
    let route = candidates[0]

    // assemble the parameters
    let parameters = {}
    let captured = url.match(route.pattern)
    for(let i = 0; i < route.capture.length; i++) { parameters[route.capture[i]] = captured[i + 1] }
    for each(let { key: k, value: [v] } in Iterator(req.getParameterMap().entrySet())) { parameters[k] = v }
    
    // call the appropriate handler
    route.fn(parameters)
}

__package__.dispatchFn = function(fn) {
    // assemble the parameters
    let parameters = {}
    for each(let { key: k, value: [v] } in Iterator(req.getParameterMap().entrySet())) { parameters[k] = v }
    
    // call the appropriate handler
    fn(parameters)
}

})
