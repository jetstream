/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
defpackage('jetstream.util', function(pkg) {

// reads in the request body as a string
pkg.getRequestBody = function() {
    let is = req.getInputStream()
    let bytes = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, 1024)
    let sb = new java.lang.StringBuffer()
    let bytesRead
    while((bytesRead = is.read(bytes)) != -1) {
        sb.append(new java.lang.String(bytes, 0, bytesRead))
    }
    return sb.toString()
}

// reads in the contents of a file in a line-oriented way and return them as a string
pkg.readFile = function(file) {
    let(contents = new java.lang.StringBuffer(),
        input = new java.io.BufferedReader(new java.io.FileReader(file)), 
        line = null) {
        while (line = input.readLine()) { contents.append(line + '\n') }
        return new String(contents)
    }
}

})
