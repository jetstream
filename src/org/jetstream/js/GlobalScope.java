/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js;

// Sun dependencies
import java.io.*;
import java.util.*;

// Mozilla dependencies
import org.mozilla.javascript.*;
import org.mozilla.javascript.debug.*;

// internal dependencies
import org.jetstream.js.*;

/**
  * GlobalScope is the top-level scope for all scripting engine instances. An instance of
  * GlobalScope will serve as the prototype of every other scope in the scripting engine
  * instance, although it will not serve as the parent of any JavaScript object. This is
  * because we want other scopes to act as though they contain all of the libraries loaded
  * into the scripting engine, but we do not want global variables set in instance scopes
  * to make it into the GlobalScope instance. The only time the GlobalScope should be
  * mutated at all is in a single-scope situation (like an interactive shell or a command-line
  * script invocation).
  *
  * <p>
  * GlobalScope serves primarily as a .js code loader. In this capacity it offers the
  * {@link GlobalScope#load} method to both running scripts and other components of the scripting engine.
  * It also offers the {@link GlobalScope#pkg} method to running scripts.
  *
  * <p>
  * GlobalScope is responsible for providing function stubbing and resolution to allow serialized
  * sessions and functions to resolve references to library code when they are deserialized.
  *
  * @see org.jetstream.js.servlet.DiskSessionManager
  * @see org.jetstream.js.servlet.Session
  */
public class GlobalScope extends ImporterTopLevel {

    // these need to be Maps of Object-to-Object because we're doing some trickery
    // with the reference types that we obtain the functions from.
    private final Map stubsToFunctions = new HashMap();
    private final Map functionsToStubs = new IdentityHashMap();
    
    private boolean initialized = false;
    public boolean isInitialized() {
        return initialized;
    }

    // prevent allocation without a context
    private GlobalScope() {}

    public GlobalScope(Context cx) {
        init(cx);
    }

    public GlobalScope(ContextFactory cxf) {
        Context cx = cxf.enterContext();
        try {
            init(cx);
        }
        finally {
            cx.exit();
        }
    }
    
    /** 
      * Sets up the standards objects and prototypes, as well as loading all of our extensions to
      * the standard object prototypes
      */
    public void init(Context cx) {
        // we do not seal the top level stuff
        initStandardObjects(cx, false);

        // our primary purpose here is to provide some functions that aren't included with Rhino
        String[] functions = { "load", "defpackage" };
        defineFunctionProperties(functions, GlobalScope.class, ScriptableObject.DONTENUM);
        initialized = true;
        
        // grab prototypes for extension
        Scriptable objectPrototype = ScriptableObject.getClassPrototype(this, "Object");
        Scriptable arrayPrototype = ScriptableObject.getClassPrototype(this, "Array");
        Scriptable stringPrototype = ScriptableObject.getClassPrototype(this, "String");

        // add any desired methods to Object.prototype here
        ScriptableObject.defineProperty(objectPrototype, "keys", new _object_prototype_keys(), ScriptableObject.DONTENUM);
        ScriptableObject.defineProperty(objectPrototype, "each", new _object_prototype_each(), ScriptableObject.DONTENUM);
        ScriptableObject.defineProperty(objectPrototype, "merge", new _object_prototype_merge(), ScriptableObject.DONTENUM);
        ScriptableObject.defineProperty(objectPrototype, "dumper", new _prototype_dumper(), ScriptableObject.DONTENUM);

        // add any desired methods to Array.prototype here
        ScriptableObject.defineProperty(arrayPrototype, "each", new _array_prototype_each(), ScriptableObject.DONTENUM);
        ScriptableObject.defineProperty(arrayPrototype, "dumper", new _prototype_dumper(), ScriptableObject.DONTENUM);

        // add any desired methods to String.prototype here
    }
    
    /**
      * Loads one or more files (specified as Strings in the args array) into the current GlobalScope.
      */
    public static void load(Context cx, Scriptable thisObj, Object[] args, Function funObj) throws NonScriptingException, FileNotFoundException, IOException {
        // check that this is only ever called on a GlobalScope 
        if(!(thisObj instanceof GlobalScope)) {
            throw new ScriptingException("load can only be called from a top-level GlobalScope");
        }

        for (int i = 0; i < args.length; i++) {
            // if any non-String argument was passed then we cannot complete
            if(! (args[i] instanceof String)) {
                throw new ScriptingException("load was called with non-String argument in position" + i);
            }
            String filename = (String)args[i]; 

            // read the file and evaluate it against the current object
            FileReader in = new FileReader(filename);
            try {
                // create and exec the script
                Script script = cx.compileReader(in, filename, 1, null);
                script.exec(cx, thisObj);

                // a DebuggableScript is required so that we can get information
                // about all of the functions defined in it, to allow for stubbing
                // and function resolution
                DebuggableScript dfn = cx.getDebuggableView(script);

                // if we are in compiled mode then we cannot do this.
                if(dfn != null) {
                    ((GlobalScope)thisObj).createStubs(filename, "", dfn);
                }
            }
            finally {
                in.close();
            }
        }
    }
    
    // use the DebuggableScript interface to recursively compile a list of all of the
    // functions loaded into the top-level scope. this is used to resolve function
    // stubs during deserialization
    private void createStubs(String scriptName, String prefix, DebuggableScript dfn) {
        FunctionStub stub = new FunctionStub(scriptName, prefix);
        stubsToFunctions.put((Object)stub, (Object)dfn);
        functionsToStubs.put((Object)dfn, (Object)stub);

        int l = dfn.getFunctionCount();
        for (int i = 0; i < l; ++i) {
            createStubs(scriptName, prefix + i + ".", dfn.getFunction(i));
        }
    }
    
    /**
      * Retrieves a FunctionStub on the basis of a Function passed in. The types are not
      * enforced here, rather the IdentityHashMap compares the functions based on reference
      * rather than value.
      */
    public Object getFunctionStub(Object o) {
        return functionsToStubs.get(o);
    }

    /**
      * Retrieves a Function on the basis of a FunctionStub passed in. Like getFunctionStub,
      * the types are handled internally because of a limitation of the Rhino API.
      */
    public Object getStubbedFunction(Object o) {
        return stubsToFunctions.get(o);
    }
    
    /**
      * Allocates a new {@link Package} and installs it in the GlobalScope.
      */
    public static void defpackage(Context cx, Scriptable thisObj, Object[] args, Function funObj) {
        // check the arguments
        if(!(args.length == 2) || !(args[0] instanceof String) || !(args[1] instanceof Function)) {
            throw new ScriptingException("package takes exactly two arguments: a string indicating a package" +
                                         " name and a function to execute in the new package");
        }
        
        // cast out the arguemnts
        String name = (String)args[0];
        Function fn = (Function)args[1];
        
        // split out the parts
        String[] parts = name.split("\\.");

        // make sure all of the leading parts are defined and are Scriptable
        Scriptable destination = ScriptableObject.getTopLevelScope(thisObj);
        for(int i = 0; i < parts.length - 1; i++) {
            // try get the object and see if it exists
            Object o = destination.get(parts[i], thisObj);
            boolean exists = (o != null && o != NOT_FOUND);

            // handle the case where it already exists and it is not a package
            if(exists && !(o instanceof Package)) {
                throw new ScriptingException(parts[i] + " was listed in a package name, but it is already defined and it is not an instance of Package");
            }
            // handle the case where it needs to be created 
            else if(!exists) {
                Package newPackage = new Package();
                destination.put(parts[i], destination, newPackage);
                destination = newPackage;
            }
            // handle the case where it already exists and it is a package
            else {
                destination = (Scriptable)o;
            }
        }
        
        // allocate and install the package itself
        Package newPackage = new Package();
        destination.put(parts[parts.length - 1], destination, newPackage);
        fn.call(cx, ScriptableObject.getTopLevelScope(thisObj), (Scriptable)newPackage, new Object[] { newPackage });
    }
    
    // Functions encountered during serialization are replaced with instances of
    // FunctionStub
    private static class FunctionStub implements Serializable {
        private final String scriptName;
        private final String functionName;

        FunctionStub(String scriptName, String functionName) {
            this.scriptName = scriptName;
            this.functionName = functionName;
        }

        public int hashCode() {
            return scriptName.hashCode() ^ functionName.hashCode();
        }

        public boolean equals(Object o) {
            if (o instanceof FunctionStub) {
                FunctionStub key = (FunctionStub) o;

                return key.functionName.equals(functionName) &&
                key.scriptName.equals(scriptName);
            }

            return false;
        }

        public String toString() {
            return "function stub:" + scriptName + "#" + functionName;
        }
    }
}

/*
 * this function adds the ability to call 'keys' on any Object. keys will return an array of all the property keys
 * in the object.
 */
class _object_prototype_keys extends BaseFunction {
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        return new NativeJavaArray(scope, thisObj.getIds());
    }
}

/*
 * this function adds the ability to call 'merge' on any Object. it takes another Object as a parameter and returns their additive union.
 * there are no side-effects.
 */
class _object_prototype_merge extends BaseFunction {
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        // first lets make sure that the argument is correct
        if(1 != args.length || !(args[0] instanceof Scriptable)) {
            throw new ScriptingException("merge can only be called with a single Scriptable object");
        }
        Scriptable merged = cx.newObject(scope);

        for(Object o : thisObj.getIds()) {
            // get the value for the given key
            if(o instanceof String) {
                String key = (String)o;
                Object value = thisObj.get(key, thisObj);
                merged.put(key, merged, value);
            }
            else {
                int i = ((Integer)o).intValue();
                Object value = thisObj.get(i, thisObj);
                merged.put(i, merged, value);
            }
        }
        
        Scriptable other = (Scriptable)args[0];
        for(Object o : other.getIds()) {
            // get the value for the given key
            if(o instanceof String) {
                String key = (String)o;
                Object value = other.get(key, other);
                merged.put(key, merged, value);
            }
            else {
                int i = ((Integer)o).intValue();
                Object value = other.get(i, other);
                merged.put(i, merged, value);
            }
        }

        return merged;
    }
}

/*
 * this function adds the ability to call 'each' on any Object. each takes a single function parameter to
 * which it passes [key, value] pairs for each enumerable key and value in the object
 */
class _object_prototype_each extends BaseFunction {
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        // first lets make sure that the argument is correct
        if(1 != args.length || !(args[0] instanceof Function)) {
            throw new ScriptingException("each can only be called with a single function argument");
        }
        
        // grab the function
        Function fn = (Function)args[0];
        
        // get all of the ids
        Object[] ids = thisObj.getIds();

        // loop the keys and values, supplying them to the function that was passed
        for(int i = 0; i < ids.length; i++) {
            Object o = ids[i];
            
            // get the value for the given key
            if(o instanceof String) {
                String key = (String)o;
                Object value = thisObj.get(key, thisObj);
                Object[] parameters = { key, value };
                fn.call(cx, scope, thisObj, parameters);
            }
            else {
                int key = ((Integer)o).intValue();
                Object value = thisObj.get(key, thisObj);
                Object[] parameters = { key, value };
                fn.call(cx, scope, thisObj, parameters);
            }
        }

        return Undefined.instance;
    }
}

class _prototype_dumper extends BaseFunction {
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        try {
            org.json.JSONStringer stringer = new org.json.JSONStringer();

            if(thisObj instanceof Scriptable && ((Scriptable)thisObj).getPrototype() == ScriptableObject.getObjectPrototype(scope)) {
                dumpObject((Scriptable)thisObj, scope, stringer, 0);
            }
            else if(thisObj instanceof Scriptable && ((Scriptable)thisObj).getPrototype() == ScriptableObject.getClassPrototype(scope, "Array")) {
                dumpArray((Scriptable)thisObj, scope, stringer, 0);
            }
            else {
                throw new ScriptingException("dumper can only be called on Objects and Arrays");
            }
            return stringer.toString();
        }
        catch(org.json.JSONException e) {
            throw new ScriptingException("could not serialize object", e);
        }
    }

    private void dumpObject(Scriptable target, Scriptable scope, org.json.JSONStringer stringer, int indent) throws org.json.JSONException {
        stringer.object();
        for(Object o : target.getIds()) {
            String key = (String)o;
            stringer.key(key);
            Object value = target.get(key, target);
            if(value instanceof Scriptable && ((Scriptable)value).getPrototype() == ScriptableObject.getObjectPrototype(scope)) {
                dumpObject((Scriptable)value, scope, stringer, indent);
            }
            else if(value instanceof Scriptable && ((Scriptable)value).getPrototype() == ScriptableObject.getClassPrototype(scope, "Array")) {
                dumpArray((Scriptable)value, scope, stringer, indent);
            }
            else {
                if(value instanceof Wrapper) { value = ((Wrapper)value).unwrap(); }
                stringer.value(value);
            }
        }
        stringer.endObject();
    }

    private void dumpArray(Scriptable target, Scriptable scope, org.json.JSONStringer stringer, int indent) throws org.json.JSONException {
        stringer.array();
        Object[] keys = target.getIds();
        for(int i = 0; i < keys.length; i++) {
            Object value = target.get(i, target);
            if(value instanceof Scriptable && ((Scriptable)value).getPrototype() == ScriptableObject.getObjectPrototype(scope)) {
                dumpObject((Scriptable)value, scope, stringer, indent);
            }
            else if(value instanceof Scriptable && ((Scriptable)value).getPrototype() == ScriptableObject.getClassPrototype(scope, "Array")) {
                dumpArray((Scriptable)value, scope, stringer, indent);
            }
            else {
                stringer.value(value);
            }
        }
        stringer.endArray();
    }
}

/*
 * this function adds the ability to call 'each' on any Array. each takes a single function parameter to
 * which it passes every value in the array, in turn
 */
class _array_prototype_each extends BaseFunction {
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        // first lets make sure that the argument is correct
        if(1 != args.length || !(args[0] instanceof Function)) {
            throw new ScriptingException("each can only be called with a single function argument");
        }
        
        // grab the function
        Function fn = (Function)args[0];
        
        // get all of the ids
        Object[] ids = thisObj.getIds();

        // loop the keys and values, supplying them to the function that was passed
        for(int i = 0; i < ids.length; i++) {
            Object o = ids[i];
            
            // get the value for the given key
            Object value;
            if(o instanceof String) {
                String key = (String)o;
                value = thisObj.get(key, thisObj);
            }
            else {
                int key = ((Integer)o).intValue();
                value = thisObj.get(key, thisObj);
            }
            
            // place the call
            Object[] parameters = { value };
            fn.call(cx, scope, thisObj, parameters);
        }
        
        // this method has no well-defined return
        return Undefined.instance;
    }
}
