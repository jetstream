/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js;

// Mozilla dependencies
import org.mozilla.javascript.*;

/**
  * Package is a Rhino host object that represents a distinct library namespace.
  * In most ways it is indistinguishable from any other JavaScript object.
  *
  * @see GlobalScope#pkg
  */
public class Package extends ScriptableObject {
    
    /**
      * Constructs an empty package. This will typically be done when the package name specified
      * to {@link GlobalScope#pkg} has parent namespaces that haven't been allocated yet.
      */
    public Package() { }
    
    /**
      * Constructs a package from the supplied JavaScript object. All of the properties of the
      * supplied object will be copied into the new package.
      *
      * @param source The Scriptable object containing the desired properties of the package.
      */
    public Package(Scriptable source) {
        // copy all of the properties of the source object into this one
        for(Object o : source.getIds()) {
            if(!(o instanceof String)) {
                throw new ScriptingException("A package must include only keys which are strings");
            }
            String key = (String)o;
            put(key, this, source.get(key, source));
        }
    }

    public String getClassName() {
        return "Package";
    }
}
