/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js;

// Mozilla dependencies
import org.mozilla.javascript.*;

/**
  * ContextMaker ensures that when {@link org.mozilla.javascript.Context} objects are
  * created that they receive the properties needed by the scripting engine
  */
public class ContextMaker extends ContextFactory {
    // we will seek the maximum available optimization unless otherwise instructed
    private int optimizationLevel = 9;
    // we will default to version 1.7
    private int languageVersion = Context.VERSION_1_7;
    
    // getters and setters
    public int getOptimizationLevel() { return optimizationLevel; }
    public void setOptimizationLevel(int i) { optimizationLevel = i; }
    public int getLanguageVersion() { return languageVersion; }
    public void setLangaugeVersion(int i) { languageVersion = i; }

    protected void onContextCreated(Context cx) {
        cx.setOptimizationLevel(optimizationLevel);
        cx.setLanguageVersion(languageVersion);
    }
    
    protected boolean hasFeature(Context cx, int featureIndex)
    {
        switch (featureIndex) {
          case Context.FEATURE_DYNAMIC_SCOPE:
            return true;

          case Context.FEATURE_UNIFORM_JAVA_EXCEPTIONS:
            return true;
        }
        return super.hasFeature(cx, featureIndex);
    }
}
