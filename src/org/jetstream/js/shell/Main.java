/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.shell;

// Sun dependencies
import java.io.*;

// Mozilla dependencies
import org.mozilla.javascript.*;

// internal dependencies
import org.jetstream.js.*;

public class Main {
    private static GlobalScope global;

    public static void main(final String[] args) {
        ContextFactory cxf = new ContextMaker();
        Context cx = cxf.enterContext();
        try {
            global = new GlobalScope(cx);
            global.load(cx, global, args, null);
        }
        catch(NonScriptingException e) {
            System.err.println(e);
        }
        catch(FileNotFoundException e) {
            System.err.println(e);
        }
        catch(IOException e) {
            System.err.println(e);
        }
        finally {
            cx.exit();
        }
    }
}
