/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js;

// Mozilla dependencies
import org.mozilla.javascript.*;

/**
  * JetStreamException is the default exception thrown when a script performs an illegal action or
  * encounters an unrecoverable error. Other such (more specific) exceptions should inherit from
  * ScriptingException.
  *
  * @see ScriptingException
  * @see NonScriptingException
  */
public class JetStreamException extends RuntimeException {
    
    public JetStreamException(String message) {
        super(message);
    }

    public JetStreamException(String message, Throwable cause) {
        super(message, cause);
    }
}
