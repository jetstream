/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js;

// Mozilla dependencies
import org.mozilla.javascript.*;

/**
  * NonScriptingException is the default exception thrown when the scripting engine
  * encounters an unrecoverable error. Other such (more specific) exceptions should 
  * inherit from NonScriptingException.
  *
  * @see ScriptingException
  */
public class NonScriptingException extends Exception {
    
    public NonScriptingException(String message) {
        super(message);
    }

    public NonScriptingException(String message, Throwable cause) {
        super(message, cause);
    }
}
