/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;
import org.mozilla.javascript.*;

public class Session extends ScriptableObject {
    // do not allow construction without supplying a global session
    private Session() {}
    
    public Session(Scriptable global) {
        setPrototype(global);
    }

    public String getClassName() {
        return "Session";
    }
}
