/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import javax.servlet.http.*;

// Mozilla dependencies
import org.mozilla.javascript.*;

// Apache dependencies
import org.apache.log4j.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

/**
  * MemorySessionManager stores user sessions and function restarts in-process. This
  * means that a reload of the servlet or container will destroy all existing sessions
  * and function restarts.
  */
public class MemorySessionManager extends SessionManager {
    // sessions
    private Map<String, byte[]> sessions;
    
    // function restarts
    private Map<String, byte[]> restarts;

    public MemorySessionManager(Application app, int maxSessions) {
        super(app);

        final int MAX_SESSIONS = maxSessions;
        sessions = new LinkedHashMap<String, byte[]>(MAX_SESSIONS+1, .75F, true) {
            protected boolean removeEldestEntry (Map.Entry<String, byte[]> eldest) {
                return size() > MAX_SESSIONS;
            }
        };

        final int MAX_RESTARTS = 10000;
        restarts = new LinkedHashMap<String, byte[]>(MAX_SESSIONS+1, .75F, true) {
            protected boolean removeEldestEntry (Map.Entry<String, byte[]> eldest) {
                return size() > MAX_RESTARTS;
            }
        };
    }
    
    // retrieve a session from the session store
    public synchronized Session getSession(String guid, HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException {
        // get from memcache
        byte[] bytes = (byte[])sessions.get(guid); 
        if(bytes == null) {
            return null;
        }
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);

        // get an input stream
        StubbingScriptableInputStream sin = new StubbingScriptableInputStream(bin, app.getGlobalScope(), app.getGlobalScope(), app, req, res);
        return (Session)sin.readObject();
    }
    
    public synchronized void commitSession(Session s) throws IOException {
        // get an output stream
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, bout, app.getGlobalScope());

        // write the object
        sout.writeObject(s);
        sout.flush();
        
        // commit it to memcache
        Scriptable inner = (Scriptable)s.get("session", s);
        String guid = (String)inner.get("guid", inner);
        sessions.put(guid, bout.toByteArray());
    }

    // retrieve a session from the session store
    public synchronized Function getFunctionRestart(String guid, HttpServletRequest req, HttpServletResponse res) 
        throws IOException, ClassNotFoundException {
        // get from memcache
        byte[] bytes = (byte[])restarts.get(guid);
        if(bytes == null) {
            return null;
        }
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);

        // get an input stream
        StubbingScriptableInputStream sin = new StubbingScriptableInputStream(bin, app.getGlobalScope(), app.getGlobalScope(), app, req, res);

        // get the function
        return (Function)sin.readObject();
    }

    // genereate a new session for a given GUID
    public synchronized void newFunctionRestart(Function fn, String guid) throws IOException {
        // get an output stream
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, bout, app.getGlobalScope());

        // write the object
        sout.writeObject(fn);
        sout.flush();
        
        // commit it to memcache
        restarts.put(guid, bout.toByteArray());
    }
}
