/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.io.*;
import java.util.*;
import javax.servlet.http.*;

// Mozilla dependencies
import org.mozilla.javascript.*;
import org.mozilla.javascript.serialize.*;

// internal dependencies
import org.jetstream.js.*;

class StubbingScriptableOutputStream extends ScriptableOutputStream {
    private Application app;

    public StubbingScriptableOutputStream(GlobalScope global, Application app, OutputStream out, Scriptable scope) throws IOException {
        super(out, scope);
        this.app = app;
    }

    protected Object replaceObject(Object obj) throws IOException {
        if(obj instanceof GlobalScope) {
            return new GlobalScopeProxy();
        }
        else if(obj instanceof Application) {
            return new ApplicationProxy();
        }
        else if(obj instanceof HttpServletRequest) {
            return new RequestProxy();
        }
        else if(obj instanceof HttpServletResponse) {
            return new ResponseProxy();
        }
        else if(app.getGlobalScope().getFunctionStub(obj) != null) {
            return app.getGlobalScope().getFunctionStub(obj);
        }

        return super.replaceObject(obj);
    }
}

class StubbingScriptableInputStream extends ScriptableInputStream {
    private Application app;
    private HttpServletRequest req;
    private HttpServletResponse res;

    public StubbingScriptableInputStream(InputStream in, Scriptable scope, GlobalScope _global, Application _app, HttpServletRequest _req, HttpServletResponse _res) throws IOException {
        super(in, scope);
        app = _app;
        req = _req;
        res = _res;
    }

    protected Object resolveObject(Object obj) throws IOException {
        if(obj instanceof GlobalScopeProxy) {
            return app.getGlobalScope();
        }
        else if(obj instanceof ApplicationProxy) {
            return app;
        }
        else if(obj instanceof RequestProxy) {
            return req;
        }
        else if(obj instanceof ResponseProxy) {
            return res;
        }
        else if(app.getGlobalScope().getStubbedFunction(obj) != null) {
            return app.getGlobalScope().getStubbedFunction(obj);
        }

        return super.resolveObject(obj);
    }
}

class GlobalScopeProxy implements Serializable {}
class ApplicationProxy implements Serializable {}
class RequestProxy implements Serializable {}
class ResponseProxy implements Serializable {}
