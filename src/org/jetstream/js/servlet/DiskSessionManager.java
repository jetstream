/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.io.*;
import java.util.*;
import javax.servlet.http.*;

// Mozilla dependencies
import org.mozilla.javascript.*;
import org.mozilla.javascript.serialize.*;

// Apache dependencies
import org.apache.log4j.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

/**
  * DiskSessionManager stores sessions on the local disk. This allows sessions to
  * survive a reload of the container or the servlet. Note that at the moment no
  * effort is made to clean up the garbage. As such, this SessionManager cannot yet
  * be considered production-ready.
  *
  * @see SessionManager
  */
public class DiskSessionManager extends SessionManager {
    private File sessionDirectory;

    public DiskSessionManager(Application app, File sessionDirectory) throws NonScriptingException {
        super(app);

        // make the directory if it doesn't already exist
        if(!sessionDirectory.isDirectory()) {
            if(!sessionDirectory.mkdirs()) {
                throw new NonScriptingException(sessionDirectory + " was specified as the session directory, but it didn't exist and couldn't be created.");
            }
        }
        this.sessionDirectory = sessionDirectory;
    }

    // retrieve a session from the session store
    public Session getSession(String guid, HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException {
        //return sessions.get(guid);
        try {
            File f = new File(sessionDirectory + "/" + guid + ".session");
            StubbingScriptableInputStream sin = new StubbingScriptableInputStream(new FileInputStream(f), app.getGlobalScope(), app.getGlobalScope(), app, req, res);
            return (Session)sin.readObject();
        }
        catch(FileNotFoundException e) {
            return null;
        }
    }
    
    // disk sessions need to be committed at the end of the request
    public void commitSession(Session s) throws IOException {
        Scriptable inner = (Scriptable)s.get("session", s);
        String guid = (String)inner.get("guid", inner);

        File f = new File(sessionDirectory.toString() + "/" + guid + ".session");
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, new FileOutputStream(f), app.getGlobalScope());
        sout.writeObject(s);
        sout.flush();
    }

    // retrieve a session from the session store
    public Function getFunctionRestart(String guid, HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException {
        try {
            File f = new File(sessionDirectory + "/" + guid + ".restart");
            StubbingScriptableInputStream sin = new StubbingScriptableInputStream(new FileInputStream(f), app.getGlobalScope(), app.getGlobalScope(), app, req, res);
            return (Function)sin.readObject();
        }
        catch(FileNotFoundException e) {
            return null;
        }
    }

    // genereate a new session for a given GUID
    public void newFunctionRestart(Function fn, String guid) throws IOException {
        File f = new File(sessionDirectory + "/" + guid + ".restart");
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, new FileOutputStream(f), app.getGlobalScope());
        sout.writeObject(fn);
        sout.flush();
    }
}
