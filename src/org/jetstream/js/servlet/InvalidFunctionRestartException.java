/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Mozilla dependencies
import org.mozilla.javascript.*;

/**
  * InvalidFunctionRestartException is the exception thrown when a function restart
  * cannot be found. This usually indicates that a server reset has taken place since
  * the function restart was generated.
  */
public class InvalidFunctionRestartException extends Exception {
    
    public InvalidFunctionRestartException() {
        super("We are sorry to inform you that your session was lost. We are working to correct the problem.");
    }

    public InvalidFunctionRestartException(String message) {
        super(message);
    }

    public InvalidFunctionRestartException(String message, Throwable cause) {
        super(message, cause);
    }
}
