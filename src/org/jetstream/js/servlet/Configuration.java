/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.util.*;
import java.io.*;

// Apache dependencies
import org.apache.log4j.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

public class Configuration {
    ScriptableServlet servlet;

    // the file which contains the handlers used to dispatch requests
    File handlerEntryPoint;
    public File getHandlerEntryPoint() { return handlerEntryPoint; }

    // these names are used to look up handlers in the handlerEntryPoint file
    public static final String HANDLER = "__HANDLER__";
    public static final String INIT_HANDLER = "__INIT__";
    public static final String GET_HANDLER = "__GET__";
    public static final String POST_HANDLER = "__POST__";
    public static final String RESTART_HANDLER = "__RESTART__";
    
    // the name used for the session cookie
    public static final String SESSION_COOKIE_NAME = "x-icc-session";

    /**
      * The destination directory for the sessions. The init parameter
      * is 'session_directory'.
      */
    private File sessionDirectory;

    /**
      * The destination directory for the application's logs. The init parameter
      * is 'log_directory'.
      */
    private String logDirectory;
    
    private String[] sessionMemcacheServerList;
    private String[] memcacheServerList;

    /**
      * Returns a new logger set up according to the servlet configuration.
      */
    public Logger newLogger() {
        // configure a logger for this application
        Properties p = new Properties();
        p.setProperty("log4j.rootLogger", "DEBUG, A1");
        p.setProperty("log4j.appender.A1", "org.apache.log4j.FileAppender");
        p.setProperty("log4j.appender.A1.file", logDirectory + "/" + servlet.getServletName() + ".log");
        p.setProperty("log4j.appender.A1.layout", "org.apache.log4j.PatternLayout");
        p.setProperty("log4j.appender.A1.layout.ConversionPattern", "%d{HH:mm:ss} %p: %m - SESSION: %X{sessionGuid}%n");
        PropertyConfigurator.configure(p);
        return Logger.getLogger("rootLogger");
    }

    /**
      * Controls which SessionManager implementation will be used for this application.
      * The init parameter is 'session_manager_type'.
      */
    public enum SessionManagerType { DISK, MEMORY, MEMCACHE }
    private SessionManagerType sessionManagerType;
    public SessionManagerType getSessionManagerType() { return sessionManagerType; }

    /**
      * Allocates a new SessionManager instance based on the type specified in the 
      * init parameter.
      */
    public SessionManager newSessionManager(GlobalScope global, Application app) throws NonScriptingException {
        switch(sessionManagerType) {
            case MEMORY:
                if(maxSessions == 0) {
                    throw new NonScriptingException("If 'memory' is specified as the session manager type, then " +
                                                    "you must also specify 'max_sessions' in the servlet configuration.");
                }
                return new MemorySessionManager(app, maxSessions);
            case DISK:
                if(sessionDirectory == null) {
                    throw new NonScriptingException("If 'disk' is specified as the session manager type, then " +
                                                    "you must also specify 'session_directory' in the servlet configuration.");
                }
                return new DiskSessionManager(app, sessionDirectory);
            case MEMCACHE:
                if(sessionMemcacheServerList == null || sessionMemcacheServerList.length == 0) {
                    throw new NonScriptingException("If 'memcache' is specified as the session manager type, then " +
                                                    "you must also specify 'session_memcache_server_list' in the servlet configuration.");
                }
                return new MemcacheSessionManager(app, sessionMemcacheServerList);
        }
        
        throw new NonScriptingException("No valid 'session_manager_type' specified in the servlet configuration");
    }


    /**
      * Controls which DBConnectionFactory implementation will be used for this application.
      * The init parameter is 'db_connection_type'.
      */
    private enum DBConnectionPoolType { POSTGRES }
    private DBConnectionPoolType dbConnectionPoolType;
    
    public DBConnectionFactory newDBConnectionFactory() throws NonScriptingException {
        switch(dbConnectionPoolType) {
            case POSTGRES:
                return new PostgresDBConnectionFactory(dbConnectionPoolSize);
        }

        throw new NonScriptingException("No valid 'db_connection_type' specified in the servlet configuration");
    }

    /**
      * If this flag is true then all of the code will be recompiled on each request.
      * This allows you to develop the application without restarting the server when
      * a code change is made. Any significant load against the server in this state
      * will cause errors and inconsistencies. The server should not be considered
      * concurrency-safe when this flag is set. The init parameter is 'reload_each_request'.
      */
    private boolean reloadEachRequest;
    public boolean getReloadEachRequest() { return reloadEachRequest; }
    
    /**
      * For SessionManagers that limit the number of sessions stored at one time, this
      * flag controls the limit. The init parameter is 'max_sessions'.
      */
    private int maxSessions;

    /**
      * For SessionManagers that limit the number of function restarts stored at one time, 
      * this flag controls the limit. The init parameter is 'max_function_restarts'.
      */
    private int maxFunctionRestarts;
    
    /**
      * Controls the maximum number of connections in the database connection pool. The
      * init parameter is 'db_connection_pool_size'.
      */
    private int dbConnectionPoolSize;

    /**
      * The servlet container will require a per-application path prefix. This prefix
      * is required in a url for it to be dispatched to the appropriate servlet.
      */
    private String pathPrefix;
    public String getPathPrefix() { return pathPrefix; }

    // do not allow construction without a servlet
    private Configuration() {}
    
    public Configuration(ScriptableServlet servlet) throws NonScriptingException {
        this.servlet = servlet;

        // handler entry point
        String filename = servlet.getServletConfig().getInitParameter("handler_entry_point");
        if(null == filename || filename.equals("")) { 
            throw new NonScriptingException("No valid 'handler_entry_point' was specified in the servlet configuration.");
        }
        handlerEntryPoint = new File(filename);

        // log directory
        logDirectory = servlet.getServletConfig().getInitParameter("log_directory");
        if(logDirectory == null || logDirectory.equals("")) {
            throw new NonScriptingException("No valid 'log_directory' was specified in the servlet configuration");
        }

        // session directory
        String sd = servlet.getServletConfig().getInitParameter("session_directory");
        if(sd != null && !sd.equals("")) {
            sessionDirectory = new File(sd);
        }
        
        // sessionMemcacheServerList
        String smsl = servlet.getServletConfig().getInitParameter("session_memcache_server_list");
        if(smsl != null && !smsl.equals("")) {
            sessionMemcacheServerList = smsl.split("\\s");
        }

        // session manager type
        String smt = servlet.getServletConfig().getInitParameter("session_manager_type");
        if(smt == null || smt.equals("")) {
            throw new NonScriptingException("No valid 'session_manager_type' was specified in the servlet configuration");
        }
        else if(smt.equalsIgnoreCase("memory")) {
            sessionManagerType = SessionManagerType.MEMORY;
        }
        else if(smt.equalsIgnoreCase("disk")) {
            sessionManagerType = SessionManagerType.DISK;
        }
        else if(smt.equalsIgnoreCase("memcache")) {
            sessionManagerType = SessionManagerType.MEMCACHE;
        }
        else {
            throw new NonScriptingException("No valid 'session_manager_type' was specified in the servlet configuration");
        }

        // db connection factory type
        String dbt = servlet.getServletConfig().getInitParameter("db_connection_type");
        if(dbt == null || dbt.equals("")) {
            throw new NonScriptingException("No valid 'db_connection_type' was specified in the servlet configuration");
        }
        else if(dbt.equalsIgnoreCase("postgres")) {
            dbConnectionPoolType = DBConnectionPoolType.POSTGRES;
        }
        else {
            throw new NonScriptingException("No valid 'db_connection_type' was specified in the servlet configuration");
        }
        
        // reload each request
        String rer = servlet.getServletConfig().getInitParameter("reload_each_request");
        if(rer == null || rer.equals("")) {
            throw new NonScriptingException("No valid 'reload_each_request' was specified in the servlet configuration");
        }
        else if(rer.equalsIgnoreCase("true")) {
            reloadEachRequest = true;
        }
        else if (rer.equalsIgnoreCase("false")) {
            reloadEachRequest = false;
        }
        else {
            throw new NonScriptingException("No valid 'reload_each_request' was specified in the servlet configuration");
        }
        
        // max sessions
        String ms = servlet.getServletConfig().getInitParameter("max_sessions");
        if(null != ms && !ms.equals("")) {
            try { maxSessions = Integer.parseInt(ms); }
            catch(Exception e) { throw new NonScriptingException("An invalid 'max_sessions' was specified in the servlet configuration"); }
        }
        else {
            maxSessions = 0;
        }

        // max function restarts
        String mfr = servlet.getServletConfig().getInitParameter("max_function_restarts");
        if(null == mfr || mfr.equals("")) {
            throw new NonScriptingException("No valid 'max_function_restarts' was specified in the servlet configuration");
        }
        try { maxFunctionRestarts = Integer.parseInt(mfr); }
        catch(Exception e) { throw new NonScriptingException("No valid 'max_function_restarts' was specified in the servlet configuration"); }
        
        // db connection pool size
        String dbcps = servlet.getServletConfig().getInitParameter("db_connection_pool_size");
        if(null == dbcps || dbcps.equals("")) {
            throw new NonScriptingException("No valid 'db_connection_pool_size' was specified in the servlet configuration");
        }
        try { maxFunctionRestarts = Integer.parseInt(dbcps); }
        catch(Exception e) { throw new NonScriptingException("No valid 'db_connection_pool_size' was specified in the servlet configuration"); }

        // path prefix
        pathPrefix = servlet.getPathPrefix();
    }
}
