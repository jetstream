/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.util.concurrent.*;
import javax.servlet.http.*;

// Mozilla dependencies
import org.mozilla.javascript.*;
import org.mozilla.javascript.serialize.*;

// Apache dependencies
import org.apache.log4j.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

public abstract class SessionManager {
    protected Application app;
    
    public SessionManager(Application app) {
        this.app = app;
    }

    // utility method to generate a new GUID for session identification
    public static String generateGUID() {
        return java.util.UUID.randomUUID().toString().replace("-", "").substring(0, 12);
    }
    
    public String getFunctionRestartURL(String guid) {
        return app.getConfig().getPathPrefix() + "/fnid/" + guid;
    }

    public String getFunctionRestartGUID(String url) {
        Matcher m = Pattern.compile("^" + app.getConfig().getPathPrefix() + "/fnid/([a-f0-9]+)$").matcher(url);
        if(m.matches()) {
            return m.group(1);
        }

        return null;
    }
    
    public String makeFunctionRestart(Function fn) throws IOException {
        String guid = generateGUID();
        newFunctionRestart(fn, guid);
        return getFunctionRestartURL(guid);
    }

    public String makeFunctionRestart(Function fn, String url) throws IOException {
        url = app.getConfig().getPathPrefix() + "/" + url;
        newFunctionRestart(fn, url);
        return url;
    }

    public Function lookupFunctionRestart(String url, HttpServletRequest req, HttpServletResponse res) 
        throws IOException, ClassNotFoundException, InvalidFunctionRestartException {
        String guid = getFunctionRestartGUID(url);
        if(guid != null) {
            return getFunctionRestart(guid, req, res);
        }

        // this might be null
        return getFunctionRestart(url, req, res);
    }
    
    // genereate a new session for a given GUID
    public Session newSession(String guid) {
        Session session = new Session(app.getGlobalScope());
        Session innerSession = new Session(null);
        innerSession.put("guid", innerSession, guid);
        session.put("session", session, innerSession);

        return session;
    }
    
    public abstract Session getSession(String guid, HttpServletRequest req, HttpServletResponse res) 
        throws IOException, ClassNotFoundException;
    public abstract void commitSession(Session s) throws IOException;

    public abstract Function getFunctionRestart(String guid, HttpServletRequest req, HttpServletResponse res) 
        throws IOException, ClassNotFoundException, InvalidFunctionRestartException;
    public abstract void newFunctionRestart(Function fn, String guid) throws IOException;
}
