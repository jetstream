/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.util.*;
import java.util.concurrent.*;

// Mozilla dependencies
import org.mozilla.javascript.*;

// Apache dependencies
import org.apache.log4j.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

public class Application extends ScriptableObject {
    // required to extend ScriptableObject
    public String getClassName() { return "Application"; }
    
    // configuration
    private Configuration config;
    public Configuration getConfig() { return config; }

    // global scope, into which we load all of the code and the default JS objects
    // this will be the termination node in the prototype chain of any and all
    // session scopes
    private GlobalScope global;
    public GlobalScope getGlobalScope() { return global; }
    
    // get a clean global scope
    public void resetGlobalScope() {
        Context cx = contextFactory.enterContext();
        try {
            global = new GlobalScope(cx);
        }
        finally {
            cx.exit();
        }
        initHostObjects();
    }
    
    private ContextMaker contextFactory;
    public ContextFactory getContextFactory() { return contextFactory; }
    
    // called to get a context which will compile functions
    public Context getCompilingContext() {
        Context cx = contextFactory.enterContext();
        
        // we can only use a compiling context if the sessions are to be stored in-process
        // TODO: get rid of this, we can't ever use compiled functions anymore
        cx.setOptimizationLevel(-1);
        return cx;
    }
       
    // called to get a context which will interpret functions
    public Context getInterpretingContext() {
        Context cx = contextFactory.enterContext();
        cx.setOptimizationLevel(-1);
        return cx;
    }

    // session handling
    SessionManager sessionManager;
    public SessionManager getSessionManager() { return sessionManager; }
    
    // hold a reference to the servlet
    ScriptableServlet servlet;

    // we want a single database connection pool manager for the whole application
    DBConnectionFactory dbConnectionFactory;

    private Application() {}
    public Application(ScriptableServlet servlet, Configuration config) throws NonScriptingException {
        this.servlet = servlet;
        this.config = config;
        
        // everything needs to be able to log
        logger = config.newLogger();

        // context factory and global scope
        contextFactory = new ContextMaker();
        ContextFactory.initGlobal(contextFactory);
        Context cx = contextFactory.enterContext();
        try {
            global = new GlobalScope(cx);
        }
        finally {
            cx.exit();
        }
        
        // session manager, and connection pool
        sessionManager = config.newSessionManager(global, this);
        dbConnectionFactory = config.newDBConnectionFactory();

        // host objects
        initHostObjects();
    }
    
    // logging
    private Logger logger;
    public Logger getLogger() {
        return logger;
    }

    // set up the host objects
    private void initHostObjects() {
        global.put("app", global, this);
        this.put("servlet", this, servlet);
        this.put("config", this, config);
        this.put("db", this, dbConnectionFactory);
        this.put("sessionManager", this, sessionManager);
        this.put("log", this, logger);

        // here is where new methods will be added to the application object
        String[] names = { "fn" };
        defineFunctionProperties(names, Application.class, ScriptableObject.DONTENUM);
    }
    
    // get a new function restart
    public static Object fn(Context cx, Scriptable thisObj, Object[] args, Function funObj) {
        if(!(thisObj instanceof Application)) {
            throw new ScriptingException("fn can only be called on an Application object");
        }
        if(!(args[0] instanceof Function)) {
            throw new ScriptingException("fn must be called with a Function argument");
        }

        Function fn = (Function)args[0];
        Application app = ((Application)thisObj);
        String url;

        try {
            if((args.length == 2) && (args[1] != null) && (args[1] instanceof String)) {
                url = app.getSessionManager().makeFunctionRestart(fn, (String)args[1]);
            }
            else {
                url = app.getSessionManager().makeFunctionRestart(fn);
            }
        }
        catch(Exception e) {
            throw new ScriptingException("Could not create a function restart", e);
        }

        return url;
    }
}
