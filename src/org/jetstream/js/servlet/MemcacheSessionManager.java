/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.io.*;
import java.util.*;
import javax.servlet.http.*;

// Mozilla dependencies
import org.mozilla.javascript.*;
import org.mozilla.javascript.serialize.*;

// Apache dependencies
import org.apache.log4j.*;

// Danga dependencies
import com.danga.MemCached.*;

// internal dependencies
import org.jetstream.js.*;
import org.jetstream.db.*;

/**
  * MemcacheSessionManager stores sessions and function restarts in memcached.
  */
public class MemcacheSessionManager extends SessionManager {
	private MemCachedClient mc;

    public MemcacheSessionManager(Application app, String[] sessionMemcacheServerList) {
        super(app);

        // socket pool
        StringBuilder sb = new StringBuilder();
        for(String s : sessionMemcacheServerList) {
            sb.append(s);
        }
        String socketPoolName = sb.toString();
		SockIOPool pool = SockIOPool.getInstance(socketPoolName);
		pool.setServers(sessionMemcacheServerList);
		pool.setInitConn(5);
		pool.setMinConn(5);
		pool.setMaxConn(250);
		pool.setMaxIdle(1000 * 60 * 60 * 6);
		pool.setMaintSleep(30);
		pool.setNagle(false);
		pool.setSocketTO(3000);
		pool.setSocketConnectTO(0);
		pool.initialize();

        // memcache
        mc = new MemCachedClient();
        mc.setPoolName(socketPoolName);
		mc.setCompressEnable(true);
		mc.setCompressThreshold(32 * 1024);
    }

    // retrieve a session from the session store
    public Session getSession(String guid, HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException {
        // get from memcache
        byte[] bytes = (byte[])mc.get(guid); 
        if(bytes == null) {
            return null;
        }
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);

        // get an input stream
        StubbingScriptableInputStream sin = new StubbingScriptableInputStream(bin, app.getGlobalScope(), app.getGlobalScope(), app, req, res);
        return (Session)sin.readObject();
    }
    
    // disk sessions need to be committed at the end of the request
    public void commitSession(Session s) throws IOException {
        // get an output stream
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, bout, app.getGlobalScope());

        // write the object
        sout.writeObject(s);
        sout.flush();
        
        // commit it to memcache
        Scriptable inner = (Scriptable)s.get("session", s);
        String guid = (String)inner.get("guid", inner);
        mc.set(guid, bout.toByteArray());
    }

    // retrieve a session from the session store
    public Function getFunctionRestart(String guid, HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException {
        // get from memcache
        byte[] bytes = (byte[])mc.get(guid);
        if(bytes == null) {
            return null;
        }
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);

        // get an input stream
        StubbingScriptableInputStream sin = new StubbingScriptableInputStream(bin, app.getGlobalScope(), app.getGlobalScope(), app, req, res);

        // get the function
        return (Function)sin.readObject();
    }

    // genereate a new session for a given GUID
    public void newFunctionRestart(Function fn, String guid) throws IOException {
        // get an output stream
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        StubbingScriptableOutputStream sout = new StubbingScriptableOutputStream(app.getGlobalScope(), app, bout, app.getGlobalScope());

        // write the object
        sout.writeObject(fn);
        sout.flush();
        
        // commit it to memcache
        mc.set(guid, bout.toByteArray());
    }
}
