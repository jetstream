/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.js.servlet;

// Sun dependencies
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

// Apache dependencies
import org.apache.log4j.*;

// Mozilla dependencies
import org.mozilla.javascript.*;

// internal dependencies
import org.jetstream.js.*;

/**
 * ScriptableServlet is a deployable servlet class which can be used to configure
 * and run JavaScript-based applications.
 * It is never referenced from JavaScript code, but is configured as the servlet class
 * in the web.xml file of a WAR application container.
 *
 * <h3>Initialization Parameters</h3>
 * The following parameters are recognized in the web.xml file:
 * <dl>
 * <dt><b>handler_entry_point</b></dt>
 * <dd>The path of the javascript source file which is the entry point for the application.
 *     This source file must contain a __HANDLER__ object,
 *     which must contain properties for either __GET__ and __POST__,
 *     which are references to the javascript functions that will be executed
 *     when a GET or POST HTTP method is invoked.
 *     If the __HANDLER__ object contains an __INIT__ property referencing a function,
 *     that function will be called when the servlet is initialized.
 * </dd>
 * <dt><b>reload_each_request</b></dt>
 * <dd>If true, the application will be reloaded at each invocation.
 *     This should only be set during development.</dd>
 * </dl>
 *
 * <h3>Execution Environment</h3>
 * During the execution of javascript code, the following objects will be available in the global scope:
 * <dl>
 * <dt><b>app</b></dt>
 * <dd></dd>
 * <dt><b>db</b></dt>
 * <dd>A reference to a org.jetstream.js.DBConnectionFactory.</dd>
 * <dt><b>sessionManager</b></dt>
 * <dd>A reference to a org.jetstream.js.servlet.SessionManager.</dd>
 * <dt><b>log</b></dt>
 * <dd>A reference to a log4j Logger.</dd>
 * </dl>
 *
 * <h3>Handler Functions</h3>
 * As described above, the javascript source file is expected to define handlers for GET and/or POST requests.
 * When one of these handler functions is invoked, the following will be available in the global scope:
 * <dl>
 * <dt><b>req</b></dt>
 * <dd>A reference to the java HttpServletRequest object.</dd>
 * <dt><b>res</b></dt>
 * <dd>A reference to the java HttpServletResponse object.</dd>
 * </dl>
 *
 * <h3>Configuration</h3>
 * TBD
 *
 */
public class ScriptableServlet extends HttpServlet {
    // handlers
    private Scriptable handler;
    private Function initHandler;
    private Function getHandler;
    private Function postHandler;
    private Function restartHandler;
    
    // the application
    private Application app;

    // set up the scope
    public void init() throws ServletException {
        try { 
            Configuration c = new Configuration(this);
            app = new Application(this, c);
        }
        catch(NonScriptingException e) { 
            throw new ServletException(e.getMessage(), e); 
        }

        Context cx = app.getCompilingContext();

        try {
            // intial load of all the values
            reload();

            // if the servlet specifies an 'init' handler then we should call it
            if(null != initHandler) {
                initHandler.call(cx, app.getGlobalScope(), null, new Object[] {});
            }
        }
        catch(FileNotFoundException e) {
            throw new ServletException(e.getMessage(), e);
        }
        catch(IOException e) {
            throw new ServletException(e.getMessage(), e);
        }
        finally {
            cx.exit();
        }
    }
    
    private enum HTTPMethod { GET, POST };

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response, HTTPMethod.GET);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response, HTTPMethod.POST);
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response, HTTPMethod method) throws ServletException, IOException {
        // first make sure that any code has been reloaded which needs it
        reloadIfNecessary();

        // enter the context
        Context cx = app.getInterpretingContext();
        try {
            // get ourselves a session
            Session session = perRequestInitialization(request, response);

            Function h = null;
            Function restart = app.getSessionManager().lookupFunctionRestart(request.getRequestURI(), request, response);
            Object[] args = {};

            // find the appropriate handler
            if(restart != null) {
                h = restartHandler;
                args = new Object[] { restart };
            }
            else if(method == HTTPMethod.GET) {
                h = getHandler;
            }
            else if(method == HTTPMethod.POST) {
                h = postHandler;
            }
            
            // if we didn't find a handler then 
            if(h == null) {
                throw new NonScriptingException("No handler found.");
            }

            // invoke the actual handler
            session.setPrototype(app.getGlobalScope());
            session.put("req", session, request);
            session.put("res", session, response);
            h.call(cx, session, null, args);
            session.delete("req");
            session.delete("res");
            
            // commit the session and stuff
            perRequestTeardown(session);
        }
        catch(FileNotFoundException e) {
            throw new ServletException(e.getMessage(), e);
        }
        catch(ClassNotFoundException e) {
            throw new ServletException(e.getMessage(), e);
        }
        catch(InvalidFunctionRestartException e) {
            throw new ServletException(e.getMessage(), e);
        }
        catch(NonScriptingException e) {
            throw new ServletException(e.getMessage(), e);
        }
        catch(IOException e) {
            throw new ServletException(e.getMessage(), e);
        }
        finally {
            cx.exit();
        }
    }

    // reload if necessary
    private void reloadIfNecessary() throws FileNotFoundException, IOException, ServletException {
        if(app.getConfig().getReloadEachRequest()) {
            reload();
        }
    }

    // reload the js
    private void reload() throws FileNotFoundException, IOException, ServletException {
        app.resetGlobalScope();
        Context cx = app.getCompilingContext();
        try {
            // intermediate value
            Object untyped;

            // load the file
            try {
                app.getGlobalScope().load(cx, app.getGlobalScope(), new Object[] { app.getConfig().getHandlerEntryPoint().toString() }, null);
            }
            catch(NonScriptingException e) {
                throw new ServletException("Non-Scripting exception", e);
            }
            
            // retrieve the handler
            untyped = app.getGlobalScope().get(Configuration.HANDLER, app.getGlobalScope());
            if(null == untyped || !(untyped instanceof Scriptable)) {
                throw new ServletException(app.getConfig().getHandlerEntryPoint() + " does not define a " + Configuration.HANDLER);
            }
            handler = (Scriptable)untyped;

            // store references to the js handlers
            untyped = handler.get(Configuration.INIT_HANDLER, handler);
            if (untyped instanceof Function) {
                initHandler = (Function) untyped;
            }
            untyped = handler.get(Configuration.GET_HANDLER, handler);
            if (untyped instanceof Function) {
                getHandler = (Function) untyped;
            }
            untyped = handler.get(Configuration.POST_HANDLER, handler);
            if (untyped instanceof Function) {
                postHandler = (Function) untyped;
            }
            untyped = handler.get(Configuration.RESTART_HANDLER, handler);
            if (untyped instanceof Function) {
                restartHandler = (Function) untyped;
            }
        }
        finally {
            cx.exit();
        }
    }
    
    // handle session initialization on a per-request basis
    private Session perRequestInitialization(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // attempt to grab the session cookie
        String guid = getCookieValue(request, Configuration.SESSION_COOKIE_NAME);
        Session session;
        try {
            session = (null == guid) ? null : app.getSessionManager().getSession(guid, request, response);
        }
        catch(ClassNotFoundException e) {
            throw new ServletException(e.getMessage(), e);
        }
        
        // if we don't have one, then let's make one
        if(null == session) {
            guid = app.getSessionManager().generateGUID();
            session = app.getSessionManager().newSession(guid);
            Cookie c = new Cookie(Configuration.SESSION_COOKIE_NAME, guid);
            c.setPath(app.getConfig().getPathPrefix());
            response.addCookie(c);
        }

        // set the session guid in the MDC
        MDC.put("sessionGuid", guid);
        
        return session;
    }
    
    // handle session teardown on a per-request basis
    private void perRequestTeardown(Session session) throws IOException {
        app.getSessionManager().commitSession(session);
    }

    // how is this not shipped with tomcat?
    private static String getCookieValue(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if(null == cookies) { return null; }

        for(Cookie c : cookies) {
            if(c.getName().equals(name)) {
                return c.getValue();
            }
        }
        return null;
    }

    // figure out our path prefix. i strongly suspect there is a better way to do this, but
    // i also have better things to do with my time than to track it down
    public String getPathPrefix() {
        String path = (new StringBuffer(getServletContext().getRealPath("/"))).reverse().toString();
        return "/" + (new StringBuffer(path.substring(1, path.indexOf("/", 1)))).reverse().toString();
    }
}
