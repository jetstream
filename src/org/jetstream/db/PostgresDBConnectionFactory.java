/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.db;

// Sun dependencies
import java.sql.*;
import javax.sql.*;
import java.util.*;

// PostreSQL dependencies
import org.postgresql.jdbc3.*;

// internal dependencies
import org.jetstream.db.*;

public class PostgresDBConnectionFactory implements DBConnectionFactory {
    // we will only allocate a given datasource once for every unique set of connection parameters
    private Map<String, Jdbc3PoolingDataSource> dsMap;
    private int connections;
    
    // basic constructor setup
    private PostgresDBConnectionFactory() {}
    public PostgresDBConnectionFactory(int maxConnections) {
        connections = maxConnections; 
        dsMap = new LinkedHashMap<String, Jdbc3PoolingDataSource>();
    }
    
    // get a new connection from the right connection pool
    public Connection getConnection(String serverAddress, String databaseName, int portNumber, String username, String password) throws SQLException {
        // compute a key to cache the datasource
        String key = serverAddress + databaseName + username + password;
        
        // we need a DataSource to get a connection
        Jdbc3PoolingDataSource ds;
    
        // this is to prevent the creation of more than one DataSource for a given connect string
        synchronized(dsMap) {
            ds = dsMap.get(key);
            
            // create a DataSource if we don't already have one
            if(ds == null) {
                ds = new Jdbc3PoolingDataSource();
                ds.setServerName(serverAddress);
                ds.setDatabaseName(databaseName);
                ds.setPortNumber(portNumber);
                ds.setUser(username);
                ds.setPassword(password);
                // connections is a property of this object
                ds.setMaxConnections(connections);
                
                // store it for next time
                dsMap.put(key, ds);
            }
        }

        // either way we've got a datasource now, so use it to get a new connection
        return ds.getConnection();
    }
}
