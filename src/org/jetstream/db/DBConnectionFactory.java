/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Copyright INTERCASTING CORP  2008
    All Rights Reserved.  Licensed Software.

    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF INTERCASTING CORP
    The copyright notice above does not evidence any actual or
    intended publication of such source code.
 
    PROPRIETARY INFORMATION, PROPERTY OF INTERCASTING CORP
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
package org.jetstream.db;

// Sun dependencies
import java.sql.*;
import javax.sql.*;
import java.util.*;

/**
  * DBConnectionFactory provides an opportunity to do connection pooling. The applications
  * running inside the scripting engine can access the database using this interface and
  * a best-effort attempt will be made to pool connections.
  */
public interface DBConnectionFactory {
    /**
      * Retrieves a connection from the connection pool.
      */
    public Connection getConnection(String serverAddress, String databaseName, int portNumber, String username, String password) throws SQLException;
}
