importClass(java.lang.System)

let(a = 1, [b, c] = [2, 3], d = 4) {
    System.out.println([a, b, c, d].join(' '))
}
