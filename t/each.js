importClass(java.lang.System)

var foo = { a: 1, b: 2 }

foo.each(function(k, v) {
    System.out.println('key:' + k + ' value: ' + v)
})


var bar = [ 1, 2, 3, 4 ]
bar.each(function(e) {
    System.out.println(e)
})
