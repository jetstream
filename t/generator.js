let gen = function() {
    yield 1
    yield 2
    yield 3
}

let f = gen()
java.lang.System.out.println(f.next())
java.lang.System.out.println(f.next())
java.lang.System.out.println(f.next())
